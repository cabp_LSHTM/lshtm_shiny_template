# Overview

Provides a skeleton for a shiny app with a [CMMID](https://cmmid.lshtm.ac.uk) brand that links to the [CMMID Project Repository Page](https://cmmid.github.io).

# HOWTO

Clone this, then copy everything into the new folder you want to use, then update the internals in the normal way for your shiny app.

You can also use make to duplicate the relevant content; navigate to your clone of this repository, then:

```
lshtm_shiny_template$ make ../your_new_shiny_app
```

# Example

This is derived from the app which comes with the [denvax](https://gitlab.com/cabp_LSHTM/denvax) package. It looks like:

![viewed desktop](desktop.png)

or preview mobile mode using the inspect feature of chrome:

![previewed on mobile](mobile-preview.png)

