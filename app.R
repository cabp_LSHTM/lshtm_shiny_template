# this provides a template for making CMMID-branded shiny apps
# key points:
#  - organized as a navbar page
#  - show audience something first: sidebar layouts are all controls-right
#    which means when viewed on mobile, the plot appears first rather than the controls
#  - the notes markdown is the place to document any long form details.

library(shiny)

apptitle <- "Example CMMID Shiny App"

# Define UI for application that draws a histogram
ui <- navbarPage(
    title = div(
        a(img(src="cmmid_newlogo.svg", height="45px"), href="https://cmmid.github.io/"), span(apptitle, style="line-height:45px")
    ),
    windowTitle = apptitle,
    theme = "styling.css",
    position="fixed-top", collapsible = TRUE,
    tabPanel("Old Faithful Example",sidebarLayout(position = "right",
        sidebarPanel(
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30)
        ),
        mainPanel(
            plotOutput("distPlot")
        )
    )),
    tabPanel("Notes", includeMarkdown("notes.md"))
)
# Define server logic required to draw a histogram
server <- function(input, output) {

    output$distPlot <- renderPlot({
        # generate bins based on input$bins from ui.R
        x    <- faithful[, 2]
        bins <- seq(min(x), max(x), length.out = input$bins + 1)

        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, col = 'darkgray', border = 'white')
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
